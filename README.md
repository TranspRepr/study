The Study
=========

Two studies are conducted: study 1 focuses on identifying agencies
and individuals that may be familiar with open science practices
and the second one looks at the prevalence of such practices among a
broader range of federal agencies and beyond.

Study 1 is presented below while [study 2 is here](http://bitbucket.org/TranspRepr/study/src/master/Study2.md)

# Study 1: Summary of the findings

* The extent of open development of tools used to produce data
  products varies greatly among BLS, CB, and GSA with the former
  having one, second having a trace amount, and the last one
  sharing most.

* It is not clear if the actual software and practices
  used to produce the official reports is openly shared even by GSA.

* A GSA rep should be invited to the workshop as the most advanced
  example to share their experiences while BLS to help understand
  why this approach is not uniform among agencies. The mission of
  data.gov also required GSA to get in contact with other agencies.

# Study 1: Objective

Discover main artifacts and associated agents who are most involved
in open science (transparency and reproducibility) within federal
statistical agencies.


# Study 1: Motivation

In order of have an impact on the progress of open science within
federal agencies producing statistical products it is critical to
involve major players (if any) who may already pursuing the agenda of
open science within these agencies. They would be likely to be
implementing whatever the decision makers resolve to do (so it is
important to involve them early) and can make progress regardless of
the administrative decision making.


# Study 1: Method

A standard operational data approach:
Discover/retrieve/contextualize/correct/impute/analyze/present cycle.

1. Discover: Google search

```
census bureau statistics open source
bls statistics open source
```

1. Inspect first three pages of results.

1. Recognize and retrieve potentially relevant sources and follow
   the links therein.

1. Store results on http://bitbucket.org/TranspRepr/study

1. Limitations

    a. Missing: the internal work within the agencies (even if using github)
    will not be visible

    b. Context: the work that is found may not be always directly
    related to statistical data products

    c. Wrong: The individuals publicizing the work externally may not be the
    ones who do it internally


## Study 1: RQ1: Major artifacts - state of open science in CB and BLS


1. No source or practices used to produce tools/software for data
products in federal agencies were found.

1. Some organizations (e.g., US CB) are aware of open source
   software development
   (https://github.com/uscensusbureau/open-source-policy)
   and provide such environment for tools that help developer
   end-users to retrieve data provided are developed in open
   manner. The list of such projects is very short
   (https://github.com/uscensusbureau). GSA has a longer list of
   projects https://github.com/GSA and even more in
   https://github.com/18F.  BLS appears to have related
   publications (Meyer, Peter B. "Episodes of collective invention." US Bureau of
Labor Statistics Working Paper 368 (2003))

1. Some tools that help developer end-users to retrieve data
provided are developed in an open manner (see, e.g, 
http://open.gsa.gov/developer/, https://github.com/uscensusbureau/citysdk).

1. The progress on providing access to data is much greater
   including developer APIs http://api.census.gov/data.html,
   http://www.bls.gov/developers/ and also fairly rich non-developer
   interfaces: http://www.census.gov/data/data-tools.html, 
   http://factfinder.census.gov/faces/nav/jsf/pages/index.xhtml,
   http://www.bls.gov/data/, http://data.gov, https://www.usa.gov/developer

1. Related topics of open government: http://www.census.gov/foia/

## Study 1: RQ2: Major players in the open innovation practices

1. CB citysdk: jeffreymeisel@gmail.com who works for
   CB. Interestingly, the most prolific contributor for the CB
   project citysdk is shrestha_tejen@bah.com from (Booz Allen
   Hamilton: a leading provider of management and technology
   consulting services to the US government in defense,
   intelligence, and civil markets)

1. BLS: Meyer, Peter B. 

1. General Services Administration

    a. Site data.gov, does not expose its internal workings but
       allows feedback, requests and provides a sketch of a workflow
       to incorporate additional data.
    a. The source of data can be traced back to original agencies
       and their developer portals. 
    a. For developer sites GSA has a much larger presence: https://github.com/GSA, https://github.com/18F


1. Updates based on the Workshop on Transparency and Reproducibility in Federal Statistics

    a.  