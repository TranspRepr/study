# Study 2: Summary of the findings

* A large number of agencies, local governments, open communities,
  and companies use open source for supporting tasks,
  like web standards, service APIs, and so on, but few (if any)
  appear to make their internal workflows of producing data
  products transparent (with the exception of USGS).

* Top users of open source are USGS, GSA, and Consumer Financial
Protection Bureau.

# Study 2: Objective

Discover agencies who are most involved in open source within
federal agencies and the surrounding communities.


# Study 2: Motivation

A broader understanding of which agencies do employ open source and what
communities exist that support federal and local governments may
help decide whom to bring into the workshop.


# Study 2: Method

Start from individuals and organizations discovered in Study 1 and
expand it by identifying additional individuals and organizations in
an iterative manner. 

1. Select GH organizations used by members in GSA and uscensus

1. Find members and their organizations

1. Iterate


# Study 2: Results (after three iterations)

## How extensive is each agency-related open-source operation?

1. Number of repositories (projects)

1. Number of people

1. Number of commits (changes)



| Agency                         | Repos | Members | Changes |
|:-------------------------------|------:|--------:|--------:|
| GSA                            |   277 |      29 |   66809 |
| department-of-veterans-affairs |   248 |      10 |   11771 |
| cfpb                           |   208 |      56 |   32054 |
| usgs                           |   123 |     142 |   27641 |
| USGS-CIDA                      |   104 |      13 |   45381 |
| USGS-R                         |    73 |      11 |   18606 |
| USDepartmentofLabor            |    56 |       5 |    1140 |
| USEPA                          |    46 |      10 |    6359 |
| WhiteHouse                     |    40 |      26 |    3553 |
| unitedstates                   |    39 |      54 |   11191 |
| USFWS                          |    34 |       4 |    5804 |
| DOE-NEPA                       |    25 |       1 |   17072 |
| usnationalarchives             |    18 |       7 |   14583 |
| usinterior                     |     9 |       3 |     714 |
| USAID                          |     8 |       1 |     103 |
| uscensusbureau                 |     7 |       4 |    3080 |
| usds                           |     6 |      27 |     893 |
| USDA-FSA                       |     5 |       2 |     904 |
| CMSgov                         |     5 |       4 |     360 |
| ombegov                        |     4 |       2 |     264 |
| FDA                            |     4 |       2 |     287 |
| USCIS                          |     3 |       6 |      91 |
| CommerceGov                    |     3 |       7 |      22 |
| government                     |     1 |     172 |       8 |
| DOE-Workflows                  |     1 |       1 |       7 |
| deptofdefense                  |     0 |       4 |       0 |


## What are the classes of government-related orgs? (from a total of 510 orgs):

1. Official government agencies

```
unitedstates: A shared commons of data and tools for the United States. Made by the public, used by the public.
usds: The United States Digital Service is transforming how the federal government works for the American people.
uscensusbureau
GSA
department-of-veterans-affairs
USDepartmentofLabor
USCIS:U.S. Citizenship and Immigration Services
CMSgov
cfpb: Consumer Financial Protection Bureau 
deptofdefense: This organization has no public repositories.
DOE-Workflows
FDA
ombegov: Office of the Federal Chief Information Officer
USDA-FSA
USAID
USEPA
USFWS
usgs
usnationalarchives
DOE-NEPA
USGS-R: R code from USGS
USGS-CIDA: USGS Center for Integrated Data Analytics
```

1. Local/State gov related communities

```
DCgov
codefordc: Code for DC is an all volunteer organization that creates tools, information, and experiences that serve the District.
codeforamerica/brigade
openstates/openstates.org-tmp
dcfemtech: The DC Women in Tech Collective
openlawdc
tfrce
NREL: National Renewable Energy Laboratory
ncbi: National Center for Biotechnology Information/NLM/NIH
CAHealthData
Chicago
cityofaustin
CityofPittsburgh
CityofToronto
CivicHall
DigitalState
```

1. Government-related Infrastructure

```
fisma-ready
18F
USG-Website-Templates
eregs: U.S. federal regulations and other regulatory information
Drupal4Gov
opengovplatform
```


1. Adjacent companies
```
DistrictDataLabs
CommerceDataService
```

1. Dedicated to open data

opencivicdata: openCivicData

1. pretty extreme
```
http://open-control.org/
https://github.com/stopblockingtheproductivity
```

1. other
```
presidential-innovation-fellows
```
1. UK sites

```
alphagov
gds-operations
```

