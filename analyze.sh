echo users/GSA | python ./ghQuery.py /repos > gsaRepo
perl -ane 's/,/\n/g;print' < gsaRepo | grep '^"full_name"' | cut -d: -f2- | sed 's/"//g' > gsaRepos
cat gsaRepos | awk '{print "repos/"$1}' | python ./ghQuery.py /commits > gsaCmts

echo users/uscensusbureau | python ./ghQuery.py /repos > censusRepo
perl -ane 's/,/\n/g;print' < censusRepo | grep '^"full_name"' | cut -d: -f2- | sed 's/"//g' > censusRepos

cat censusRepos | awk '{print "repos/"$1}' | python ./ghQuery.py /commits > censusCmts
cat censusCmts | perl -ane 's/,/\n/g;print' | grep '"commit":{"author":{"name":"' | sed 's|"commit":{"author":{"name":"||;s|"||'  | sort | uniq -c | sort -n | tail
     29 Marina Martin
     39 Tor N. Johnson
     90 Tejen Shrestha
    102 Joe Germuska
    114 Ian Dees
    117 Haley Van Dyck
    144 Philip Ashlock
    144 Ross Dakin
    727 Ryan Pitts
   1143 Gray Brooks


   https://github.com/gbinal: is in GSA


for o in uscensusbureau  GSA department-of-veterans-affairs USDepartmentofLabor CommerceGov USCIS CMSgov usds unitedstates government \
			    usinterior WhiteHouse cfpb deptofdefense DOE-Workflows FDA ombegov USDA-FSA USAID USEPA USFWS usgs usnationalarchives DOE-NEPA
do echo orgs/$o | python ./ghQuery.py /members > $o.members
  cat $o.members | perl -ane 's/,/\n/g;print' | grep '{"login":"'| sed 's/.*{"login":"//;s/"//;s|^|users/|' | python ./ghQuery.py /orgs > $o.morgs
  cat $o.members | perl -ane 's/,/\n/g;print' | grep '{"login":"'| sed 's/.*{"login":"//;s/"//;s|^|users/|' | python ./ghQuery.py > $o.users
done



perl -ane 's/,/\n/g;print' *.morgs | grep login | sed 's|.*login":"||' | sort | uniq -c | sort -n | less


https://github.com/government/welcome: "collaborative community for sharing best practices in furtherance of open source, open data, and open government efforts. To encourage open dialog, the community is a semi-private community, with only government employees having access."
https://github.com/unitedstates: A shared commons of data and tools for the United States. Made by the public, used by the public.
https://github.com/usds: The United States Digital Service is transforming how the federal government works for the American people.
uscensusbureau
GSA
department-of-veterans-affairs
USDepartmentofLabor
CommerceGov
USCIS:U.S. Citizenship and Immigration Services
CMSgov
cfpb: Consumer Financial Protection Bureau 
deptofdefense: This organization has no public repositories.
DOE-Workflows
FDA
ombegov: Office of the Federal Chief Information Officer
USDA-FSA
USAID
USEPA
USFWS
usgs
usnationalarchives
DOE-NEPA
USGS-R
USGS-CIDA

https://github.com/WhiteHouse/budgetdata: e.g., The data behind the Presidents 2016 Budget
https://github.com/USAJOBS
#local gov related communities
DCgov
https://github.com/codefordc: Code for DC is an all volunteer organization that creates tools, information, and experiences that serve the District.
https://github.com/codeforamerica/brigade
https://github.com/openstates/openstates.org-tmp
https://github.com/dcfemtech: The DC Women in Tech Collective
openlawdc
https://github.com/tfrce
https://github.com/NREL: National Renewable Energy Laboratory
https://github.com/ncbi: National Center for Biotechnology Information/NLM/NIH
https://github.com/CAHealthData
Chicago
cityofaustin
CityofPittsburgh
CityofToronto
CivicHall
DigitalState

#Infrastructure
fisma-ready
18F
USG-Website-Templates
eregs: U.S. federal regulations and other regulatory information
Drupal4Gov
https://github.com/opengovplatform

#adjacent companies
https://github.com/DistrictDataLabs
https://github.com/CommerceDataService

#Dedicated to open data
https://github.com/opencivicdata: openCivicData

#pretty extreme
http://open-control.org/
https://github.com/stopblockingtheproductivity

#other
https://presidentialinnovationfellows.gov/fellows
https://github.com/presidential-innovation-fellows

#UK sites
https://github.com/alphagov
https://github.com/gds-operations